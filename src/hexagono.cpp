#include "../inc/hexagono.hpp"
#include <iostream>
#include <math.h>

Hexagono::Hexagono(){
    set_tipo("Hexagono");
    set_base(5.0);
}
Hexagono::Hexagono(float lado){
    set_tipo("Hexagono");
    set_base(lado);
}
Hexagono::~Hexagono(){
    //cout << "Destruindo o objeto: " << get_tipo() << endl;
}
float Hexagono::calcula_area(){
    return (3 * pow(get_base(), 2) * sqrt(3)) / 2;
}
float Hexagono::calcula_perimetro(){
    return get_base() * 6;
}