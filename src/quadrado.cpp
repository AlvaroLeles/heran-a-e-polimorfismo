#include "../inc/quadrado.hpp"
#include <iostream>
#include <math.h>

Quadrado::Quadrado(){
    set_tipo("Quadrado");
    set_base(10.0);
    set_altura(get_base());
}
Quadrado::Quadrado(float lado){
    set_tipo("Quadrado");
    set_base(lado);
    set_altura(lado);
}
Quadrado::~Quadrado(){
    //cout << "Destruindo o objeto: " << get_tipo() << endl;
}
float Quadrado::calcula_area(){
    return pow(get_base(), 2);
}
float Quadrado::calcula_perimetro(){
    return get_base() * 4;
}