#include "../inc/paralelogramo.hpp"
#include <iostream>
#include <math.h>

Paralelogramo::Paralelogramo(){
    set_tipo("Paralelogramo");
    set_ladoBase(8.0);
    set_ladoAltura(5.0);
    set_altura(4.0);
}
Paralelogramo::Paralelogramo(float ladoBase, float ladoAltura, float altura){
    set_tipo("Paralelogramo");
    set_ladoBase(ladoBase);
    set_ladoAltura(ladoAltura);
    set_altura(altura);
}
Paralelogramo::~Paralelogramo(){
    //cout << "Destruindo o objeto: " << get_tipo() << endl;
}
void Paralelogramo::set_ladoBase(float comprimento)
{
    if(comprimento <= 0)
        throw(1);
    else
        ladoBase = comprimento;
}
float Paralelogramo::get_ladoBase()
{
    return ladoBase;
}
void Paralelogramo::set_ladoAltura(float comprimento)
{
    if (comprimento <=0)
        throw(1);
    else
        ladoAltura = comprimento;
}
float Paralelogramo::get_ladoAltura()
{
    return ladoAltura;
}
float Paralelogramo::calcula_area(){
    return get_ladoBase() * get_altura();
}
float Paralelogramo::calcula_perimetro(){
    return 2 * get_ladoBase() + 2 * get_ladoAltura();
}