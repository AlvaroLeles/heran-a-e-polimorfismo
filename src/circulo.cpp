#include "../inc/circulo.hpp"
#include <iostream>
#include <math.h>

float pi = 3.14;

Circulo::Circulo(){
    set_tipo("Circulo");
    set_raio(5.0);
}
Circulo::Circulo(float raio){
    set_tipo("Circulo");
    set_raio(raio);
}
Circulo::~Circulo(){
    //cout << "Destruindo o objeto: " << get_tipo() << endl;
}
void Circulo::set_raio(float raio)
{
    if (raio <= 0)
        throw(1);
    else
        this->raio = raio;
    
}
float Circulo::get_raio()
{
    return raio;
}
float Circulo::calcula_area(){
    return pi * pow(get_raio(), 2);
}
float Circulo::calcula_perimetro(){
    return 2 * pi * get_raio();
}