#include <iostream>
#include <vector>
#include "../inc/formageometrica.hpp"
#include "../inc/circulo.hpp"
#include "../inc/hexagono.hpp"
#include "../inc/paralelogramo.hpp"
#include "../inc/pentagono.hpp"
#include "../inc/quadrado.hpp"
#include "../inc/triangulo.hpp"

using namespace std;

int main()
{
    Circulo circulo;
    FormaGeometrica formaGenerica;
    Hexagono hexagono;
    Paralelogramo paralelogramo;
    Pentagono pentagono;
    Quadrado quadrado;
    Triangulo triangulo;

    vector<FormaGeometrica *> formas;

    formas.push_back(&circulo);
    formas.push_back(&formaGenerica);
    formas.push_back(&hexagono);
    formas.push_back(&paralelogramo);
    formas.push_back(&pentagono);
    formas.push_back(&quadrado);
    formas.push_back(&triangulo);

    for (int i = 0; i < formas.size(); i++)
    {
        cout << "Tipo: " << formas[i]->get_tipo() << endl;
        cout << "Área: " << formas[i]->calcula_area() << endl;
        cout << "Perímetro : " << formas[i]->calcula_perimetro() << endl;
    }
    
    
    return 0;
}