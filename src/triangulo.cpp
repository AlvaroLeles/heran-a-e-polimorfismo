#include "../inc/triangulo.hpp"
#include <iostream>
#include <math.h>

Triangulo::Triangulo(){
    set_tipo("Triângulo");
    set_base(23.0);
    set_altura(12.0);
}
Triangulo::Triangulo(float base, float altura){
    set_tipo("Triângulo");
    set_base(base);
    set_altura(altura);
}
Triangulo::~Triangulo(){
    //cout << "Destruindo o objeto: " << get_tipo() << endl;
}
float Triangulo::calcula_area(){
    return (get_base() * get_altura()) / 2;
}
float Triangulo::calcula_perimetro(){
    return get_base() + get_altura() + (sqrt(pow(get_base(), 2) + pow(get_altura(), 2)));
}