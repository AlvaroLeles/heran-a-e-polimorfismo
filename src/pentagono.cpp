#include "../inc/pentagono.hpp"
#include <iostream>
#include <math.h>

Pentagono::Pentagono(){
    set_tipo("Pentagono");
    set_base(5.0);
    set_apotema(7.0);
}
Pentagono::Pentagono(float lado, float apotema){
    set_tipo("Pentagono");
    set_base(lado);
    set_apotema(apotema);
}
Pentagono::~Pentagono(){
    //cout << "Destruindo o objeto: " << get_tipo() << endl;
}
void Pentagono::set_apotema(float apotema)
{
    if (apotema <= 0)
        throw(1);
    else
        this->apotema = apotema;
}
float Pentagono::get_apotema()
{
    return apotema;
}
float Pentagono::calcula_area(){
    return calcula_perimetro() * get_apotema() / 2;
}
float Pentagono::calcula_perimetro(){
    return get_base() * 5;
}