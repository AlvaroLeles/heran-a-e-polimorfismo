#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formageometrica.hpp"
#include <string>

using namespace std;

class Quadrado : public FormaGeometrica {
public:
    Quadrado();
    Quadrado(float lado);
    ~Quadrado();
    virtual float calcula_area();
    virtual float calcula_perimetro();
};

#endif