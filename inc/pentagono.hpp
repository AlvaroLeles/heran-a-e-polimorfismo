#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include "formageometrica.hpp"
#include <string>

using namespace std;

class Pentagono : public FormaGeometrica {
private:
    float apotema;
public:
    Pentagono();
    Pentagono(float lado, float apotema);
    ~Pentagono();
    void set_apotema(float apotema);
    float get_apotema();
    virtual float calcula_area();
    virtual float calcula_perimetro();
};

#endif