#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include "formageometrica.hpp"
#include <string>

using namespace std;

class Hexagono : public FormaGeometrica {
public:
    Hexagono();
    Hexagono(float lado);
    ~Hexagono();
    virtual float calcula_area();
    virtual float calcula_perimetro();
};

#endif