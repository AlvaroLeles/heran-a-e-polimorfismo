#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include "formageometrica.hpp"
#include <string>

using namespace std;

class Paralelogramo : public FormaGeometrica {
private:
    float ladoBase;
    float ladoAltura;
public:
    Paralelogramo();
    Paralelogramo(float ladoBase, float ladoAltura, float altura);
    ~Paralelogramo();
    void set_ladoBase(float comprimento);
    float get_ladoBase();
    void set_ladoAltura(float comprimento);
    float get_ladoAltura();
    virtual float calcula_area();
    virtual float calcula_perimetro();
};

#endif