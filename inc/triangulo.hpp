#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include "formageometrica.hpp"
#include <string>

using namespace std;

class Triangulo : public FormaGeometrica {
public:
    Triangulo();
    Triangulo(float base, float altura);
    ~Triangulo();
    virtual float calcula_area();
    virtual float calcula_perimetro();
};

#endif